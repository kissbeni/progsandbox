﻿using System;
using System.IO;
using System.Text;
using ExecutionEngine;
using Nancy.ModelBinding;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RestService
{
	public class ExecInfo
	{
		public string code;
		public string stdin;
	}

	public class RestModule : Nancy.NancyModule
    {
		public RestModule()
		{
			Get["/ace.js"] = _ => File.ReadAllText("www/ace/ace.js");
			Get["/theme-monokai.js"] = _ => File.ReadAllText("www/ace/theme-monokai.js");
			Get["/mode-c_cpp.js"] = _ => File.ReadAllText("www/ace/mode-c_cpp.js");
			         
			Get["/"] = _ => File.ReadAllText("www/home.html");
         
			Post["/exec/cpp"] = _ =>
			{
				var x = this.Bind<ExecInfo>();

				Engine engine = new Engine();
                InputData idata = new InputData()
                {
					Program = x.code,
					Lang = Languages.CPP,
					Stdin = x.stdin
                };

				OutputData output = engine.DoWork(idata);

				return JsonConvert.SerializeObject(output, Formatting.Indented);
			};
        }
    }
}
