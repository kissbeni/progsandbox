﻿using System;
using System.Diagnostics;
using System.Threading;
using Nancy.Hosting.Self;

namespace RestService
{
    class MainClass
    {
        public static void Main(string[] args)
        {
			RestModule module = new RestModule();
			using (var nancyHost = new NancyHost(new Uri("http://localhost:8888/")))
            {
				nancyHost.Start();

                Console.WriteLine("Nancy now listening - navigating to http://localhost:8888/. Press enter to stop");

				while (true) {
					Thread.Sleep(250);
				}
            }
        }
    }
}
