﻿using System;
using ExecutionEngine.Compilers;

namespace ExecutionEngine
{
	public enum Languages
    {
        Java,
        Python3,
        Python,
        C,
        CPP
    }

    public static class LanguageUtil
    {
		public static string GetLangFileExtension(Languages lang)
		{
			switch (lang)
            {
                case Languages.Java:    return ".java";
                case Languages.Python:  return ".py";
                case Languages.C:       return ".c";
                case Languages.CPP:     return ".cpp";
                default:                return ".unknown";
            }
		}

		public static BaseSourceCompiler GetCompilerForLang(Languages lang)
		{
			switch (lang)
            {
                case Languages.Java:
					return new JavaCompiler();
                case Languages.C:
                    return new CCompiler();
                case Languages.CPP:
					return new CppCompiler();
                case Languages.Python:
					return new InterpretedLangFakeCompiler("python2.7");
                case Languages.Python3:
					return new InterpretedLangFakeCompiler("python3");
                default:
					return null;
            }
		}
    }
}
