﻿using System;
using System.IO;
using System.Text;
using System.Threading;

namespace ExecutionEngine
{
	public class OutputReader
    {
        StreamReader reader;

        public string Output { get; set; }

		public StringBuilder Builder { get; private set; } = new StringBuilder();

        private int CheckInterval { get; set; }

        public OutputReader(StreamReader reader, int interval = 10)
        {
            this.reader = reader;
            this.CheckInterval = interval;
        }

        public void ReadOutput()
        {
            try
            {
                int bufferSize = 40000;
                byte[] buffer = new byte[bufferSize];
                int outputLimit = 200000;
                int count;
                bool addMore = true;

                while (true)
                {
                    Thread.Sleep(CheckInterval);
                    count = reader.BaseStream.Read(buffer, 0, bufferSize);

                    if (count == 0)
                        break;

                    if (addMore)
                    {
						Builder.Append(Encoding.UTF8.GetString(buffer, 0, count));

						if (Builder.Length > outputLimit)
                        {
							Builder.Append("\n\n...");
                            addMore = false;
                        }
                    }
                }

				Output = Builder.ToString();
            }
            catch (Exception e)
            {
                Output = string.Format("Error while reading output: {0}", e.Message);
            }
        }
    }
}
