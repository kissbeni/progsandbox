using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using System.IO.Compression;
using System.Runtime.InteropServices;
using ExecutionEngine.Compilers;
using System.Linq;

namespace ExecutionEngine
{   
    public class InputData
    {
        public string Program { get; set; }
		public string Stdin   { get; set; }
        public Languages Lang { get; set; }
    }

    public class OutputData
    {
        public string Output        { get; set; }
        public string Errors        { get; set; }
        public string Warnings      { get; set; }
        public string Stats         { get; set; }
        public string Exit_Status   { get; set; }
        public int ExitCode         { get; set; }
        public string System_Error  { get; set; }
    }
    
    public class Engine
    {
		[DllImport("libc", SetLastError = true)]
        private static extern int chmod(string pathname, int mode);

        public const string RootPath = "/tmp/progsandbox/";
        
        private static readonly Random rng = new Random();
        
        public OutputData DoWork(InputData idata)
        {
			File.Copy("parent.py", "/tmp/progsandbox-sandbox.py", true);
			chmod("/tmp/progsandbox-sandbox.py", 0xFFF);

			if (!Directory.Exists("/tmp/progsandbox/"))
				Directory.CreateDirectory("/tmp/progsandbox/");

            CompilerData cdata = null;
            try
            {
                OutputData odata = new OutputData();
                cdata = CreateExecutable(idata);
                if (!cdata.Success)
                {
                    odata.Errors = cdata.Error;
                    odata.Warnings = cdata.Warning;
                    odata.Stats = string.Format("Compilation time: {0} sec", Math.Round(cdata.CompileTimeMs / 1000.0, 2));
                    return odata;
                }

                if (!string.IsNullOrEmpty(cdata.Warning))
                    odata.Warnings = cdata.Warning;

                Stopwatch watch = new Stopwatch();
                watch.Start();
                using (Process process = new Process())
                {
					process.StartInfo.FileName = "unshare";
					process.StartInfo.Arguments = "--mount --uts --ipc --net --pid --fork --user python2 /tmp/progsandbox-sandbox.py " + cdata.Executor + (string.IsNullOrEmpty(cdata.Executor) ? "" : " ") + cdata.ExecuteThis;
					Console.WriteLine("{0} {1}", process.StartInfo.FileName, process.StartInfo.Arguments);
					process.StartInfo.UseShellExecute = false;
                    process.StartInfo.CreateNoWindow = true;
                    process.StartInfo.RedirectStandardError = true;
                    process.StartInfo.RedirectStandardOutput = true;
					process.StartInfo.RedirectStandardInput = true;

                    process.Start();
     
                    OutputReader output = new OutputReader(process.StandardOutput);
                    Thread outputReader = new Thread(new ThreadStart(output.ReadOutput));
                    outputReader.Start();
                    
                    OutputReader error = new OutputReader(process.StandardError);
                    Thread errorReader = new Thread(new ThreadStart(error.ReadOutput));
                    errorReader.Start();

					using (StreamReader r = new StreamReader(cdata.StdinFile))
					{
						String ln;
						while ((ln = r.ReadLine()) != null)
							process.StandardInput.WriteLine(ln);
					}

					process.StandardInput.Flush();

                    process.WaitForExit();

                    errorReader.Join(5000);
                    outputReader.Join(5000);

                    if (!string.IsNullOrEmpty(error.Output))
                    {
                        int index = error.Output.LastIndexOf('\n');
                        int exitcode;

                        if (index != -1 && index + 1 < error.Output.Length && Int32.TryParse(error.Output.Substring(index + 1), out exitcode))
                        {
                            odata.ExitCode = exitcode;
                            switch (exitcode)
                            {
                                case -8:
                                    odata.Exit_Status = "Floating point exception (SIGFPE)";
                                    break;
                                case -9:
                                    odata.Exit_Status = "Kill signal (SIGKILL)";
                                    break;
                                case -11:
                                    odata.Exit_Status = "Invalid memory reference (SIGSEGV)";
                                    break;
                                case -6:
                                    odata.Exit_Status = "Abort signal from abort(3) (SIGABRT)";
                                    break;
                                case -4:
                                    odata.Exit_Status = "Illegal instruction (SIGILL)";
                                    break;
                                case -13:
                                    odata.Exit_Status = "Broken pipe: write to pipe with no readers (SIGPIPE)";
                                    break;
                                case -14:
                                    odata.Exit_Status = "Timer signal from alarm(2) (SIGALRM)";
                                    break;
                                case -15:
                                    odata.Exit_Status = "Termination signal (SIGTERM)";
                                    break;
                                case -19:
                                    odata.Exit_Status = "Stop process (SIGSTOP)";
                                    break;
                                case -17:
                                    odata.Exit_Status = "Child stopped or terminated (SIGCHLD)";
                                    break;
                                default:
                                    odata.Exit_Status = string.Format("Exit code: {0}", exitcode);
                                    break;
                            }

                            error.Output = error.Output.Substring(0, index);
                        }
                    }

                    odata.Errors = error.Output;
                    odata.Output = output.Output;
                }

                watch.Stop();

                if (idata.Lang != Languages.Python)
                    odata.Stats = string.Format("Compilation time: {0} sec, absolute running time: {1} sec", Math.Round(cdata.CompileTimeMs / 1000.0, 2), Math.Round(watch.ElapsedMilliseconds / 1000.0, 2));
                else
                    odata.Stats = string.Format("Absolute running time: {0} sec", Math.Round(watch.ElapsedMilliseconds / 1000.0, 2));

                return odata;
            }
            catch (Exception ex)
            {
                return new OutputData()
                {
                    System_Error = ex.Message
                };
            }
            finally
            {
                if (cdata != null)
                    Cleanup(cdata.WorkDir);
            }
        }

        private void Cleanup(string dir)
        {
            try
            {
                // cleanup
                Directory.Delete(dir, true);
			}
			catch (Exception e)
			{
				Console.Error.WriteLine(e);
			}
        }

        CompilerData CreateExecutable(InputData input)
        {
			string rand = RandomString();
            string dir = rand + "/";
            
			string PathToSource = RootPath + dir + rand + LanguageUtil.GetLangFileExtension(input.Lang);
            Directory.CreateDirectory(RootPath + dir);

            using (TextWriter sw = new StreamWriter(PathToSource))
                sw.Write(input.Program);

			using (TextWriter sw = new StreamWriter(PathToSource + ".input"))
				sw.Write(input.Stdin);

            CompilerData cdata = new CompilerData();
			cdata.StdinFile = PathToSource + ".input";
			cdata.PathToSource = PathToSource;
			cdata.WorkDir = RootPath + dir;
			cdata.Success = false;

			BaseSourceCompiler compiler = LanguageUtil.GetCompilerForLang(input.Lang);

			if (compiler != null)
				compiler.Compile(cdata);
			else
				cdata.Error = "Compiler not found!";

			if (cdata.Error != null)
				cdata.Error = cdata.Error.Replace(cdata.PathToSource, "");

			if (cdata.Warning != null)
				cdata.Warning = cdata.Warning.Replace(cdata.PathToSource, "");

            return cdata;
        }

        private string RandomString()
        {
			const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(chars, 64)
              .Select(s => s[rng.Next(s.Length)]).ToArray());
        }
    }
}
