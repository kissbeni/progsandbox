﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace ExecutionEngine.Compilers
{
	public class CompilerData
    {
		public long CompileTimeMs;
		public string PathToSource;
		public string WorkDir;
		public bool Success;
		public string Warning;
		public string Error;
		public string Executor;
		public string ExecuteThis;
		public string StdinFile;
    }
	
	public abstract class BaseSourceCompiler
    {      
		protected List<string> CallCompiler(string compiler, string args, out long compileTime)
        {
            Stopwatch watch = new Stopwatch();

            using (Process process = new Process())
            {
                process.StartInfo.FileName = compiler;
                process.StartInfo.Arguments = args;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardOutput = true;

                watch.Start();
                process.Start();

                OutputReader output = new OutputReader(process.StandardOutput, 100);
                Thread outputReader = new Thread(new ThreadStart(output.ReadOutput));
                outputReader.Start();

                OutputReader error = new OutputReader(process.StandardError, 100);
                Thread errorReader = new Thread(new ThreadStart(error.ReadOutput));
                errorReader.Start();

                process.WaitForExit();
                watch.Stop();

				compileTime = watch.ElapsedMilliseconds;
                errorReader.Join(5000);
                outputReader.Join(5000);

                List<string> compOutput = new List<string>();
                compOutput.Add(output.Output);
                compOutput.Add(error.Output);
                return compOutput;
            }
        }

		protected string ConcatenateString(string a, string b)
        {
            if (string.IsNullOrEmpty(a))
                return b;

            if (string.IsNullOrEmpty(b))
                return a;

            return a + "\n\n" + b;
        }

		public abstract void Compile(CompilerData cdata);
    }
}
