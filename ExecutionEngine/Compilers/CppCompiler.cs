﻿using System;
using System.IO;
using System.Collections.Generic;

namespace ExecutionEngine.Compilers
{
	public class CppCompiler : BaseSourceCompiler
	{
		public override void Compile(CompilerData cdata)
		{
			List<string> res = CallCompiler(
				"g++",
				"-fno-gnu-keywords " +
				"-fno-asm " +
				"-Dasm=error " +
				"-D__asm__=error " +
				"-Dsystem=__disabled_function__system " +
				"-Dexecl=__disabled_function__execl " +
				"-Dexeclp=__disabled_function__execlp " +
				"-Dexecle=__disabled_function__execle " +
				"-Dexecv=__disabled_function__execve " +
				"-Dexecvp=__disabled_function__execvp " +
				"-Dexecvpe=__disabled_function__execvpe " +
				"-Wall " +
				"-o " + cdata.WorkDir + "a.out " +
				cdata.PathToSource,
				out cdata.CompileTimeMs
			);

			if (!File.Exists(cdata.WorkDir + "a.out"))
            {
                if (res.Count > 1)
                {
                    cdata.Error = ConcatenateString(res[0], res[1]);
                    if (cdata.Error != null)
                    {
                        string[] ew = cdata.Error.Split(new string[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
                        string error = "";
                        string warning = "";

                        foreach (var a in ew)
                            if (a.Contains("error: "))
                                error += a + "\n";
                            else if (a.Contains("warning: "))
                                warning += a + "\n";

                        cdata.Error = error;
                        cdata.Warning = warning;
                    }
                }

                cdata.Success = false;
                return;
            }

            if (res.Count > 1 && (!string.IsNullOrEmpty(res[0]) || !string.IsNullOrEmpty(res[1])))
                cdata.Warning = ConcatenateString(res[0], res[1]);

			cdata.ExecuteThis = cdata.WorkDir + "a.out";
            cdata.Executor = "";
            cdata.Success = true;
		}
	}
}
