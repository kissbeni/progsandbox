﻿using System;
namespace ExecutionEngine.Compilers
{
	public class InterpretedLangFakeCompiler : BaseSourceCompiler
    {
		public string Interpreter { get; private set; }

		public InterpretedLangFakeCompiler(string interpreterCmd)
        {
			Interpreter = interpreterCmd;
        }

		public override void Compile(CompilerData cdata)
		{
			cdata.ExecuteThis = cdata.PathToSource;
			cdata.Executor = Interpreter;
            cdata.Success = true;
		}
	}
}
