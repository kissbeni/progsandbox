﻿using System;
using System.IO;
using System.Collections.Generic;

namespace ExecutionEngine.Compilers
{
	public class JavaCompiler : BaseSourceCompiler
    {      
		public override void Compile(CompilerData cdata)
		{
			List<string> res = CallCompiler("javac", " -Xlint -encoding UTF-8 " + cdata.PathToSource, out cdata.CompileTimeMs);

			if (!File.Exists(cdata.WorkDir + "Rextester.class"))
            {
                if (res.Count > 1)
                {
                    if (string.IsNullOrEmpty(res[0]) && string.IsNullOrEmpty(res[1]))
                        cdata.Error = "Entry class 'Rextester' not found.";
                    else
                        cdata.Error = ConcatenateString(res[0], res[1]);
                }

                cdata.Success = false;
				return;
            }

            if (res.Count > 1 && (!string.IsNullOrEmpty(res[0]) || !string.IsNullOrEmpty(res[1])))
                cdata.Warning = ConcatenateString(res[0], res[1]);

			cdata.ExecuteThis = "-Xms10m -Xmx32m -Dfile.encoding=UTF-8 -classpath " + cdata.WorkDir + " Rextester";
            cdata.Executor = "java";
            cdata.Success = true;
		}
	}
}
