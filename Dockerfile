FROM ubuntu

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y mono-complete gcc python2.7 python ash
RUN /bin/ash -c "rm /bin/bash; exit 0"
RUN /bin/ash -c "rm /bin/sh; exit 0"

COPY ./RestService/bin/Debug/* /psb/
COPY ./parent.py /psb/
COPY ./www/ /psb/www/

WORKDIR /psb/

EXPOSE 8888

CMD ["/usr/bin/mono", "/psb/RestService.exe"]
